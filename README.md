This project is the data analysis project for Document Modelling.  
The file SumUP.py has all the functions required to run the project, from reading data, wrangling to classification.  
To read in the documents, change the database name and project id in Demo.py file, while the database name was the database with all the documents, and project id was the id of the project containing all the document data. If documents in multiple projects, please refer to the branch [multi_projects](Multiple projects).  
To run the program, run the Demo.py file. In command line run this file like  
```python
Python3 Demo.py database_name project_id threshold
```  
Where `database_name` is a String, which is the name of the database where the documents are, `project_id` String is the id of the project where all the documents are. `threshold` is the threshold of the title clustering, as a double.  
It would produce two files, one named Data_Wragling.csv, which is a csv file with the necessary information from the raw data. It is to be used for analysis. The other is named Category_clauses.csv, which is the file recording the classification result.  
To visually view the result of the project, run the visualisation file in jupyter notebook. Each branch only contains one visualisation file, which would be the graph of that special branch.   

Branches:  
[master](master)  
[multi_projects](Multiple projects)  
[with_edges](Visualisation with edges)  
[line_thickness](Visualisation with line thickness)  
