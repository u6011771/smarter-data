import psycopg2
import csv
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.decomposition import PCA
# import matplotlib.pyplot as plt
from numpy import *
from sklearn.cluster import DBSCAN, KMeans, Birch
from sklearn.metrics import silhouette_score
from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.tokenize import sent_tokenize, word_tokenize
ps = PorterStemmer()
lemmatizer = WordNetLemmatizer()




def dataTransformation(database_name, project_id):
    """
    Data transformation into a csv file
    output: csv file with all the clauses in the database, attributes are:
    clauseID|clause content|DocName|Category|OtherCategories
    :return: the csv file of all data, Data_Wrangling.csv
    """
    out_file = "Data_Wrangling"
    try:
        conn = psycopg2.connect("dbname='"+ database_name +"' user='postgres' "
                                "host='intern-2018-icedb.ctrkgktgfaqq.ap-southeast-2.rds.amazonaws.com' "
                                "password='p*stgr35'")
    except:
        print("Lost database connection")

    cur = conn.cursor()

    try:
        cur.execute(
            "select * from projects where id = '" + project_id + "'")
    except:
        print("Database command error")
    row1 = cur.fetchall()
    # This is trying to get the project id, matters, client and version.
    # We need to write them for each of the clauses.
    # print(row1)
    row1_content = row1[0]
    principle = row1_content[0]
    client_name = row1_content[6]
    matter_name = row1_content[7]
    project_name = row1_content[8]
    version_name = row1_content[9]

    try:
        cur.execute(
            "select * from elements where body->>'ElementType'='Document'"
            " and principal='" + principle + "'"
        )
    except:
        print("Database command error")
    # Extract every document

    rows = cur.fetchall()

    doc_id_list = []
    title_list_3 = []
    doc_name_list = []
    for row in rows:
        # print(row[3])
        doc_id_list.append(row[2])  # All the document ids
        title_list_3.append(row[4]['Title'])

    all_document_list = []
    for n in range(len(doc_id_list)):
        doc_name = title_list_3[n]
        doc_name_list.append(doc_name)
        document_n = []

        ############### doc_type ###################
        # doc_type = idDocType(doc_name)

        command = "select * from marshalling_elementasrootgethandler('"+ principle +"', '" + project_id + "', '" + \
                  doc_id_list[n] + "')"
        try:
            cur.execute(command)
        except:
            print("Database command error")

        rows2 = cur.fetchall()


        json_data = rows2[0][0]  # how you get the json data

        if json_data is None:
            return None

        dictionary_data = json_data['Elements']
        if dictionary_data is not None:

            """
            There are two types of data structures in the documents.
            One of them has Numbered List as titles, followed by a textchunk as the content. This type of document has
            all its clauses ids recorded in num_lst_ids_list.
            The other of them has clauses recorded in SemanticeType = "Clause". This type of document has all its 
            clauses ids recorded in clauses_ids_list.
            Also, for each type of document, there are two types of data structure. One of them is that, the Document 
            element has children element with SemanticType="Clause", and those elements have the clauses contens in 
            their children. The other is that Document element has children element with semanticType = "Title", while 
            this "Title" children further has their children element with SemanticType="Clause". Thus those two types 
            were separately added into the ids lists. 
            """

            num_lst_ids_list = []
            for ids2 in json_data['Elements']:  # They are documents
                if 'SemanticType' in json_data['Elements'][ids2]:
                    if json_data['Elements'][ids2]['SemanticType'] == 'Numbered List':
                        num_lst_ids_list.append(ids2)
                    elif json_data['Elements'][ids2]['SemanticType'] == 'Title':
                        idstem2 = json_data['Elements'][ids2]['Children']
                        for idtem2 in idstem2:
                            if 'SemanticType' in json_data['Elements'][idtem2]:
                                if json_data['Elements'][idtem2]['SemanticType'] == 'Numbered List':
                                    if idtem2 not in num_lst_ids_list:
                                        num_lst_ids_list.append(idtem2)

            clauses_ids_list = []
            for ids in dictionary_data:  # They are documents
                if 'SemanticType' in dictionary_data[ids]:
                    if dictionary_data[ids]['SemanticType'] == 'Clause':
                        clauses_ids_list.append(ids)
                    elif dictionary_data[ids]['SemanticType'] == 'Title':
                        idstem = dictionary_data[ids]['Children']
                        for idtem in idstem:
                            if 'SemanticType' in dictionary_data[idtem]:
                                if dictionary_data[idtem]['SemanticType'] == 'Clause':
                                    clauses_ids_list.append(idtem)

            id_content = {}
            categoryname_clauseids = {}
            categoryid_name = {}
            for ids in clauses_ids_list:
                onlycontent = ""
                if 'Children' in dictionary_data[ids]:
                    children0 = dictionary_data[ids]['Children']
                    x0 = children0[0]
                    if 'Contents' in dictionary_data[x0]:
                        onlycontent = dictionary_data[x0]['Contents']


                # parents_ids_list = [""]
                parent_id = dictionary_data[ids]['Parent']
                # print(parent_id)
                parents_ids_list = [parent_id]
                if parent_id in dictionary_data:
                    while 'Parent' in dictionary_data[parent_id]:
                        parent_id = dictionary_data[parent_id]['Parent']
                        # print(parent_id)
                        parents_ids_list.append(parent_id)
                        if parent_id not in dictionary_data:
                            break

                # print(doc_name, ids, parents_ids_list)

                tem_list = parents_ids_list[:-1]
                tem_list.insert(0, ids)


                for tem_ids in tem_list:
                    if 'SemanticType' in dictionary_data[tem_ids]:
                        if dictionary_data[tem_ids]['SemanticType'] == 'Numbered List':
                            if 'Children' in dictionary_data[tem_ids]:
                                children = dictionary_data[tem_ids]['Children']
                                x = children[0]
                                text0 = ""
                                if 'Contents' in dictionary_data[x]:
                                    text0 = dictionary_data[x]['Contents']
                                number_category = text0 # right: the category with a number list.
                                categoryid_name[tem_ids] = number_category

                            if tem_ids not in categoryname_clauseids.keys():
                                categoryname_clauseids[tem_ids] = [ids]
                            else:
                                categoryname_clauseids[tem_ids].append(ids)
                            # categoryname_clauseids records the number_list ids associated with all the clauses under
                            # this number list.

                id_content[ids] = onlycontent

            other_number_list = {}
            title_list = list(set(num_lst_ids_list))
            for ids0 in title_list:
                # print(ids0)
                paragraph = ""
                if "Children" in json_data["Elements"][ids0]:
                    children1 = json_data["Elements"][ids0]["Children"]
                    x1 = children1[0]
                    # The content of the first children of the number lists are the titles.

                    titlex = ""
                    if "Contents" in json_data["Elements"][x1]:
                        titlex = json_data["Elements"][x1]["Contents"]

                    if len(titlex.split()) > 10: # If it is a title or not
                        title1 = ""
                        y1 = children1[0:]
                    else:
                        title1 = titlex
                        y1 = children1[1:]
                        # print("title:" + title)
                        # id_title.add((ids, title))

                    for cs in y1:
                        # print("cs: " + cs)
                        if "ElementType" in json_data["Elements"][cs] and "Contents" in json_data["Elements"][cs]:
                            if json_data["Elements"][cs]["ElementType"] == "Paragraph":
                                # Clauses are not included as there is no "Contents" in clauses - only in paragraphs.
                                new_p = json_data["Elements"][cs]["Contents"]
                                paragraph = paragraph + " " + new_p

                if ids0 not in categoryname_clauseids.keys():
                    other_number_list[ids0] = paragraph
                    if paragraph:
                        document_n.append((ids0, paragraph, doc_name, title1,
                                          [project_name, client_name, matter_name, version_name]))


            for key in categoryname_clauseids.keys():
                allcontent = "" # all content of a numbered list
                allcatname = categoryid_name[key]
                for clauses in categoryname_clauseids[key]:
                    allcontent += id_content[clauses]

                document_n.append((key, allcontent, doc_name, allcatname,
                                        [project_name, client_name, matter_name, version_name]))

            # output.close()
            all_document_list.append(document_n)

    fout = open(out_file + ".csv", "w")
    csv_out = csv.writer(fout)
    for documents in all_document_list:
        for items in documents:
            csv_out.writerow(items)
    fout.close()


def sentence_stem(sentence):
    """
    Perform NLP tokenize, stemming lemmazation of a sentence.
    :param sentence: a String to be processed
    :return: The NLP processed sentence, a String.
    """
    l = []
    words = word_tokenize(sentence)
    for w in words:
        w_processed = lemmatizer.lemmatize(w)
        l.append(ps.stem(w_processed))

    return " ".join(l)


def titleCluster(threshold):
    """
    To cluster the clauses into groups, that in each group, the clauses are of the same document type, and under the same categories.
    :param threshold: threshold for the title comparison
    :return: a file containing groups and the clauses inside them.
    """

    filename = "Data_Wrangling"
    train_file = open(filename + ".csv")
    csv_read = csv.reader(train_file)

    catogory = []
    ids = []
    contents = []
    doctypes = []
    othercat = []

    cat = []

    for data in csv_read:
        # data = lines.split("|||")
        # subtitle.append(data[4])
        catogory.append(data[3])
        ids.append(data[0])
        contents.append(data[1])
        doctypes.append(data[2])
        othercat.append(data[-1][2:-3].split("', '"))

    for w in catogory:
        if w is not None:
            cat.append(sentence_stem(w))

    if cat:
        tfidf_vectorizer = TfidfVectorizer(
            # stop_words='english',
                                           use_idf=True)
        tfidf_matrix = tfidf_vectorizer.fit_transform(cat)

        duplication_set = set()
        id_title_dic = {}  # This is the dictionary of the titles, might be later used for the group labels.
        id_class_dic = {}

        # test_data = []

        with open('Category_clauses.csv', 'w') as out:
            csv_out = csv.writer(out)
            csv_out.writerow(['field name', 'clause IDs'])

            sim_to_sentence = {}

            k = 0

            result = noTitleCluster(filename)

            for i in range(len(cat)):
                if ids[i] not in duplication_set and cat[i]:
                    sim_to_sentence[i] = cosine_similarity(tfidf_matrix[i], tfidf_matrix)[0]
                    # This is the comparison of all the catogory to the i-th one.

                    same_to_first = [ids[i]]
                    id_title_dic[ids[i]] = cat[i]
                    id_class_dic[ids[i]] = k + 1
                    for j in range(len(sim_to_sentence[i])):

                        if not wetherHaveTitle(cat[i].split()) or not wetherHaveTitle(cat[j].split()): # Title or not justification
                            if ids[i] in result.keys() and ids[j] in result.keys():
                                if result[ids[i]] == result[ids[j]]:
                                        # and doctypes[i] == doctypes[j]\
                                    same_to_first.append(ids[j])
                                    duplication_set.add(ids[j])

                        elif sim_to_sentence[i][j] > threshold:
                                # and tfidfForList(othercat[i], othercat[j], threshold)
                                # To compare other categories

                            # cosine distance of clause i and j < threshold then put in the same group
                            ######## JUSTIFICATION ###############
                            same_to_first.append(ids[j]) # change!!!
                            # same_to_first = same_to_first[1:]
                            # del same_to_first[1]
                            duplication_set.add(ids[j])
                            id_title_dic[ids[j]] = cat[i]
                            id_class_dic[ids[j]] = k + 1

                    if len(same_to_first) > 1:
                        same_to_first = same_to_first[1:]

                    csv_out.writerow((catogory[i], same_to_first))
                    k += 1

                elif not cat[i]:
                    id_class_dic[ids[i]] = 0
                    id_title_dic[ids[i]] = ""


def noTitleCluster(filename):
    """
    This is to define if the two read in clauses are under the same category, if their category names are not actually
    titles but long contents
    :param filename: the file where data stored.
    :return: a dictionary of clauses id to their clusered group
    """
    fout0 = open(filename + ".csv", "r")
    reader = csv.reader(fout0)
    ids = []
    content = []
    titles = []
    subtitle = []

    # next(fout0)

    id_content = {}

    for data in reader:
        # data = lines.split("|||")
        if not wetherHaveTitle(data[3].split()):   # Title or not justification
        # titles.append(data[2])
            ids.append(data[0])
        # content.append(data[1])
            titles.append(data[3])
            # subtitle.append(data[4])

            content.append(data[3] + " " + data[1])
        # doctypes.append(data[1])
        # id_content[data[0]] = data[1]

    vectorizer = TfidfVectorizer(max_df=0.5, max_features=50,
                                 min_df=2, stop_words='english',
                                 use_idf=True)
    X = vectorizer.fit_transform(content)

    terms = vectorizer.get_feature_names()

    method = 'kmeans'
    if method == 'kmeans':
        max_sil = 0
        k0 = 0
        for k in range(2, len(content)):
            m = KMeans(
                n_clusters=k,
                init='k-means++',
                max_iter=500,
                n_init=1,
                verbose=False)
            m.fit(X)
            labels = m.labels_.tolist()

            # Sum of distances of samples to their closest cluster center
            interia = m.inertia_
            sil_coeff = silhouette_score(X, labels, metric='euclidean')

            if sil_coeff > max_sil:
                max_sil = sil_coeff
                m0 = m
                k0 = k
    elif method == 'birch':
        m = Birch(n_clusters=3,
                  threshold=0.5,
                  branching_factor=50)
    elif method == 'dbscan':
        m = DBSCAN(eps=1, min_samples=1, metric='euclidean')

    # m.fit(X)

    if method == 'birch':
        cluster_centers = m0.subcluster_centers_
    elif method == 'kmeans':
        cluster_centers = m0.cluster_centers_

    order_centroids = cluster_centers.argsort()[:, ::-1]
    # get cluster labels (0, 1, 2...) and claster terms (mpst common tags)
    # print(order_centroids)
    clusters = m0.labels_.tolist()
    cluster_ids = set(clusters)
    cluster_num = len(cluster_ids)
    cluster_terms = [[terms[ind] for ind in order_centroids[i, :5]] for i in range(k0)]

    _X = X.toarray()
    pca = PCA(n_components=2).fit(_X)
    data2D = pca.transform(_X)
    # print(len(data2D))

    centers2D = pca.transform(cluster_centers)

    cluster_data = []
    for cluster_id in cluster_ids:
        a_cluster = {
            'cluster_id': cluster_id,
            'cluster_terms': cluster_terms[cluster_id],
            'cluster_name': '-'.join(cluster_terms[cluster_id]),
            'centroid_coord': centers2D[cluster_id].tolist() if centers2D is not None else None,
            'document_data': [
                {'document_id': ids[n],
                 'document_name': content[n],
                 'coord': data2D[n].tolist()} for n, i in enumerate(clusters) if i == cluster_id]
        }
        #     print(a_cluster['document_data'])
        cluster_data.append(a_cluster)

    result = {}
    for cl in cluster_data:
        # list0 = []
        # print('=' * 50)
        # print('Cluster#%d: %s' % (cl['cluster_id'], cl['cluster_name']))
        # print('-' * 50)
        for doc in cl['document_data']:
            # list0.append(doc['document_id'])
            # print('  id=%s, %s' % (doc['document_id'], doc['document_name']))
            result[doc['document_id']] = cl['cluster_id']

        # if c1 in list0 and c2 in list0:
        #     return True

    return result


def wetherHaveTitle(l):
    """
    To give if the read in list (split of a string) is a decent title or not
    :param l: list read in
    :return: True if there is a decent title, false if not.
    """

    return len(l) <= 10


# print(expressionComparison("6f1419cc-57a6-423c-9226-b22da94d66aa", "774e4c57-08e4-4865-af62-6264a8638e67", 0.8))
